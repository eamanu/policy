# SOME DESCRIPTIVE TITLE.
# Copyright (C) 1996, 1997, 1998 Ian Jackson, Christian Schwarz, 1998-2017,
# The Debian Policy Mailing List
# This file is distributed under the same license as the Debian Policy
# Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Debian Policy Manual 4.1.6.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-08-02 11:56+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.4.0\n"

#: ../../ap-pkg-sourcepkg.rst:2
msgid "Source packages (from old Packaging Manual)"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:4
msgid ""
"The Debian binary packages in the distribution are generated from Debian "
"sources, which are in a special format to assist the easy and automatic "
"building of binaries."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:11
msgid "Tools for processing source packages"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:13
msgid ""
"Various tools are provided for manipulating source packages; they pack "
"and unpack sources and help build of binary packages and help manage the "
"distribution of new versions."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:17
msgid ""
"They are introduced and typical uses described here; see dpkg-source(1) "
"for full documentation about their arguments and operation."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:20
msgid ""
"For examples of how to construct a Debian source package, and how to use "
"those utilities that are used by Debian source packages, please see the "
"``hello`` example package."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:27
msgid "``dpkg-source`` - packs and unpacks Debian source packages"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:29
msgid ""
"This program is frequently used by hand, and is also called from package-"
"independent automated building scripts such as ``dpkg-buildpackage``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:33
msgid "To unpack a package it is typically invoked with"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:39
msgid ""
"with the ``filename.tar.gz`` and ``filename.diff.gz`` (if applicable) in "
"the same directory. It unpacks into ``package-version``, and if "
"applicable ``package-version.orig``, in the current directory."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:43
msgid "To create a packed source archive it is typically invoked:"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:49
msgid ""
"This will create the ``.dsc``, ``.tar.gz`` and ``.diff.gz`` (if "
"appropriate) in the current directory. ``dpkg-source`` does not clean the"
" source tree first - this must be done separately if it is required."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:53
msgid "See also :ref:`s-pkg-sourcearchives`."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:58
msgid "``dpkg-buildpackage`` - overall package-building control script"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:60
msgid "See ``dpkg-buildpackage(1)``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:65
msgid "``dpkg-gencontrol`` - generates binary package control files"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:67
msgid ""
"This program is usually called from ``debian/rules`` (see "
"`section\\_title <#s-pkg-sourcetree>`__) in the top level of the source "
"tree."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:71
msgid ""
"This is usually done just before the files and directories in the "
"temporary directory tree where the package is being built have their "
"permissions and ownerships set and the package is constructed using "
"``dpkg-deb/``.  [#]_"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:76
msgid ""
"``dpkg-gencontrol`` must be called after all the files which are to go "
"into the package have been placed in the temporary build directory, so "
"that its calculation of the installed size of a package is correct."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:80
msgid ""
"It is also necessary for ``dpkg-gencontrol`` to be run after ``dpkg-"
"shlibdeps`` so that the variable substitutions created by ``dpkg-"
"shlibdeps`` in ``debian/substvars`` are available."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:84
msgid ""
"For a package which generates only one binary package, and which builds "
"it in ``debian/tmp`` relative to the top of the source package, it is "
"usually sufficient to call ``dpkg-gencontrol``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:88
msgid "Sources which build several binaries will typically need something like:"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:94
msgid ""
"The ``-P`` tells ``dpkg-gencontrol`` that the package is being built in a"
" non-default directory, and the ``-p`` tells it which package's control "
"file should be generated."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:98
msgid ""
"``dpkg-gencontrol`` also adds information to the list of files in "
"``debian/files``, for the benefit of (for example) a future invocation of"
" ``dpkg-genchanges``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:105
msgid "``dpkg-shlibdeps`` - calculates shared library dependencies"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:107
msgid "See ``dpkg-shlibdeps(1)``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:112
msgid "``dpkg-distaddfile`` - adds a file to ``debian/files``"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:114
msgid ""
"Some packages' uploads need to include files other than the source and "
"binary package files."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:117
msgid ""
"``dpkg-distaddfile`` adds a file to the ``debian/files`` file so that it "
"will be included in the ``.changes`` file when ``dpkg-genchanges`` is "
"run."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:121
msgid "It is usually invoked from the ``binary`` target of ``debian/rules``:"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:127
msgid ""
"The filename is relative to the directory where ``dpkg-genchanges`` will "
"expect to find it - this is usually the directory above the top level of "
"the source tree. The ``debian/rules`` target should put the file there "
"just before or just after calling ``dpkg-distaddfile``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:132
msgid ""
"The section and priority are passed unchanged into the resulting "
"``.changes`` file."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:138
msgid "``dpkg-genchanges`` - generates a ``.changes`` upload control file"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:140
msgid "See ``dpkg-genchanges(1)``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:145
msgid "``dpkg-parsechangelog`` - produces parsed representation of a changelog"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:147
msgid "See ``dpkg-parsechangelog(1)``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:152
msgid "``dpkg-architecture`` - information about the build and host system"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:154
msgid "See ``dpkg-architecture(1)``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:159
msgid "The Debian package source tree"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:161
msgid ""
"The source archive scheme described later is intended to allow a Debian "
"package source tree with some associated control information to be "
"reproduced and transported easily. The Debian package source tree is a "
"version of the original program with certain files added for the benefit "
"of the packaging process, and with any other changes required made to the"
" rest of the source code and installation scripts."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:168
msgid ""
"The extra files created for Debian are in the subdirectory ``debian`` of "
"the top level of the Debian package source tree. They are described "
"below."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:175
msgid "``debian/rules`` - the main building script"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:177
msgid "See :ref:`s-debianrules`."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:182
msgid "``debian/substvars`` and variable substitutions"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:184
msgid "See :ref:`s-substvars`."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:189
msgid "``debian/files``"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:191
msgid "See :ref:`s-debianfiles`."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:196
msgid "``debian/tmp``"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:198
msgid ""
"This is the default temporary location for the construction of binary "
"packages by the ``binary`` target. The directory ``tmp`` serves as the "
"root of the file system tree as it is being constructed (for example, by "
"using the package's upstream makefiles install targets and redirecting "
"the output there), and it also contains the ``DEBIAN`` subdirectory. See "
":ref:`s-pkg-bincreating`."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:205
msgid ""
"This is only a default and can be easily overridden. Most packaging tools"
" no longer use ``debian/tmp``, instead preferring ``debian/pkg`` for the "
"common case of a source package building only one binary package. Such "
"tools usually only use ``debian/tmp`` as a temporary staging area for "
"built files and do not construct packages from it."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:211
msgid ""
"If several binary packages are generated from the same source tree, it is"
" usual to use a separate ``debian/pkg`` directory for each binary package"
" as the temporary construction locations."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:215
msgid ""
"Whatever temporary directories are created and used by the ``binary`` "
"target must of course be removed by the ``clean`` target."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:221
msgid "Source packages as archives"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:223
msgid ""
"As it exists on the FTP site, a Debian source package consists of three "
"related files. You must have the right versions of all three to be able "
"to use them."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:230
msgid "Debian source control file - ``.dsc``"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:228
msgid ""
"This file is a control file used by ``dpkg-source`` to extract a source "
"package. See :ref:`s-debiansourcecontrolfiles`."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:234
msgid "Original source archive - ``package_upstream-version.orig.tar.gz``"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:233
msgid ""
"This is a compressed (with ``gzip -9``) ``tar`` file containing the "
"source code from the upstream authors of the program."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:249
msgid "Debian package diff - ``package_upstream_version-revision.diff.gz``"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:237
msgid ""
"This is a unified context diff (``diff -u``) giving the changes which are"
" required to turn the original source into the Debian source. These "
"changes may only include editing and creating plain files. The "
"permissions of files, the targets of symbolic links and the "
"characteristics of special files or pipes may not be changed and no files"
" may be removed or renamed."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:244
msgid ""
"All the directories in the diff must exist, except the ``debian`` "
"subdirectory of the top of the source tree, which will be created by "
"``dpkg-source`` if necessary when unpacking."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:248
msgid ""
"The ``dpkg-source`` program will automatically make the ``debian/rules`` "
"file executable (see below)."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:251
msgid ""
"If there is no original source code - for example, if the package is "
"specially prepared for Debian or the Debian maintainer is the same as the"
" upstream maintainer - the format is slightly different: then there is no"
" diff, and the tarfile is named ``package_version.tar.gz``, and "
"preferably contains a directory named ``package-version``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:260
msgid "Unpacking a Debian source package without ``dpkg-source``"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:262
msgid ""
"``dpkg-source -x`` is the recommended way to unpack a Debian source "
"package. However, if it is not available it is possible to unpack a "
"Debian source archive as follows:"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:266
msgid "Untar the tarfile, which will create a ``.orig`` directory."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:268
msgid "Rename the ``.orig`` directory to ``package-version``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:270
msgid "Create the subdirectory ``debian`` at the top of the source tree."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:272
msgid "Apply the diff using ``patch -p0``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:274
msgid ""
"Untar the tarfile again if you want a copy of the original source code "
"alongside the Debian version."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:277
msgid ""
"It is not possible to generate a valid Debian source archive without "
"using ``dpkg-source``. In particular, attempting to use ``diff`` directly"
" to generate the ``.diff.gz`` file will not work."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:284
msgid "Restrictions on objects in source packages"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:286
msgid ""
"The source package may not contain any hard links, [#]_ [#]_ device "
"special files, sockets or setuid or setgid files.  [#]_"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:289
msgid ""
"The source packaging tools manage the changes between the original and "
"Debian source using ``diff`` and ``patch``. Turning the original source "
"tree as included in the ``.orig.tar.gz`` into the Debian package source "
"must not involve any changes which cannot be handled by these tools. "
"Problematic changes which cause ``dpkg-source`` to halt with an error "
"when building the source package are:"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:296
msgid "Adding or removing symbolic links, sockets or pipes."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:298
msgid "Changing the targets of symbolic links."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:300
msgid "Creating directories, other than ``debian``."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:302
msgid "Changes to the contents of binary files."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:304
msgid ""
"Changes which cause ``dpkg-source`` to print a warning but continue "
"anyway are:"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:307
msgid "Removing files, directories or symlinks.  [#]_"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:309
msgid ""
"Changed text files which are missing the usual final newline (either in "
"the original or the modified source tree)."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:312
msgid ""
"Changes which are not represented, but which are not detected by ``dpkg-"
"source``, are:"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:315
msgid ""
"Changing the permissions of files (other than ``debian/rules``) and "
"directories."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:318
msgid ""
"The ``debian`` directory and ``debian/rules`` are handled specially by "
"``dpkg-source`` - before applying the changes it will create the "
"``debian`` directory, and afterwards it will make ``debian/rules`` world-"
"executable."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:324
msgid ""
"This is so that the control file which is produced has the right "
"permissions"
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:328
msgid ""
"This is not currently detected when building source packages, but only "
"when extracting them."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:332
msgid ""
"Hard links may be permitted at some point in the future, but would "
"require a fair amount of work."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:336
msgid "Setgid directories are allowed."
msgstr ""

#: ../../ap-pkg-sourcepkg.rst:339
msgid ""
"Renaming a file is not treated specially - it is seen as the removal of "
"the old file (which generates a warning, but is otherwise ignored), and "
"the creation of the new one."
msgstr ""

